﻿using CursValutar.Models;
using CursValutar.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CursValutar
{
    public partial class MainPage : ContentPage
    {
        List<Curs> listCurs;
        public MainPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            listCurs = await new SursaDate().obtineListaCurs();
            listViewCurs.ItemsSource = listCurs;
        }

        private void Setari_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SetariPage());
        }
    }
}
