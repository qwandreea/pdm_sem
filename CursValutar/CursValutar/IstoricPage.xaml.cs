﻿using CursValutar.Models;
using CursValutar.Services;
using Microcharts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CursValutar
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IstoricPage : ContentPage
    {
        public IstoricPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            List<Curs> listaCurs = await new SursaDate().obtineListaCurs();
            List<ChartEntry> dateGrafic = new List<ChartEntry>();

            foreach(Curs c in listaCurs)
            {
                dateGrafic.Add(new ChartEntry((float)c.Valoare)
                {
                    Label = c.Data,
                    ValueLabel = c.Valoare.ToString(),
                    Color = new SkiaSharp.SKColor(255, 0, 0)
                }); ;
            }

            chart.Chart = new LineChart() { Entries = dateGrafic };
        }
    }
}