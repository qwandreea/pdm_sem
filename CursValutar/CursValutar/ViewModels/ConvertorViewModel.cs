﻿using CursValutar.Models;
using CursValutar.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CursValutar.ViewModels
{
    class ConvertorViewModel : INotifyPropertyChanged
    {
        public double suma { get; set; }

        private double rezultat;
        public ICommand convertesteCommand { get; set; }
        public List<Curs> ListaCurs { get; set; }
        public Curs cursSursa { get; set; }
        public Curs cursDest { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public double Rezultat { 
            get
            {
                return rezultat;
            }
            set
            {
                rezultat = value;
                NotifyPropertyChanged();
            }
        }
        
        public ConvertorViewModel()
        {
            convertesteCommand = new Command(convertesteButtonClicked);
            suma = 100;
            initializeazaListaCurs();
        }

        public async void initializeazaListaCurs()
        {
            ListaCurs = await new SursaDate().obtineListaCurs();
            cursSursa = ListaCurs[0];
            cursDest = ListaCurs[1];
        }
        private void convertesteButtonClicked()
        {
            Rezultat = suma * cursSursa.Multiplicator * cursSursa.Valoare / (cursDest.Multiplicator * cursDest.Valoare);
        }

        private void NotifyPropertyChanged([CallerMemberName]String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
