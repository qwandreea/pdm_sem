﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CursValutar.Models;
using SQLite;

namespace CursValutar.Services
{
    class DaoCurs
    {

        SQLiteConnection connection;
        
        public DaoCurs()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),"curs_valutar.db");
            connection = new SQLiteConnection(path);
            connection.CreateTable<Curs>();
        }

        public int adaugaCurs(Curs curs)
        {
            return connection.Insert(curs);
        }

        public int adaugaListaCurs(List<Curs> listCurs)
        {
            return connection.InsertAll(listCurs);
        }

        public List<Curs> obtineCursDinData(string data)
        {
            return connection.Query<Curs>("SELECT * FROM Curs WHERE data=?",data);
        }
    }
}
