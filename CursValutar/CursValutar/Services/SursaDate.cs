﻿using CursValutar.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CursValutar.Services
{
    class SursaDate
    {
        List<Curs> listCurs = new List<Curs>();

        public async Task<List<Curs>> obtineListaCurs()
        {
            /*listCurs.Add(new Curs() { Valuta = "EUR", Valoare = 4.98, Data = "2021-10-06" });
            listCurs.Add(new Curs() { Valuta = "USD", Valoare = 4.23, Data = "2021-10-06" });
            listCurs.Add(new Curs() { Valuta = "GBP", Valoare = 4.11, Data = "2021-10-06", Multiplicator = 100 });*/

            DaoCurs dao = new DaoCurs();
            List<Curs> listaCurs = dao.obtineCursDinData(Curs.obtineDataReferinta(DateTime.Now).ToString("yyyy-MM-dd"));
            if(listaCurs.Count == 0)
            {
                listaCurs =  await getBNR_XML();
                dao.adaugaListaCurs(listCurs);
            }
            return listaCurs;
        }

        private async Task <List<Curs>> getBNR_XML()
        {
            List<Curs> listCurs = new List<Curs>();
            HttpClient httpClient = new HttpClient();
            Stream stream = await httpClient.GetStreamAsync("https://bnr.ro/nbrfxrates.xml");
            using (XmlReader reader = XmlReader.Create(stream))
            {
                string data = "";
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        if(reader.Name == "Cube")
                        {
                            data = reader["date"];
                        }

                        if(reader.Name == "Rate")
                        {
                            Curs curs = new Curs() { Valuta = reader["currency"], Data = data };
                            if(reader["multiplier"] != null)
                            {
                                curs.Multiplicator = int.Parse(reader["multiplier"]);
                            }
                            reader.Read();
                            curs.Valoare = double.Parse(reader.Value);
                            listCurs.Add(curs);
                        }
                        
                    }
                }
            }
            return listCurs;
             
        }
    }
}
