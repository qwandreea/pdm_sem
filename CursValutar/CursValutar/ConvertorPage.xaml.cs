﻿using CursValutar.Models;
using CursValutar.Services;
using CursValutar.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CursValutar
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConvertorPage : ContentPage
    {
        ConvertorViewModel viewModel;

        public ConvertorPage()
        {
            InitializeComponent();
            viewModel = new ConvertorViewModel();
            BindingContext = viewModel;
        }
    }
}